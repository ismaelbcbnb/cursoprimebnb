import { Component, Input, OnInit } from '@angular/core';
import { Aula } from '../aula';

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css']
})
export class ClassComponent implements OnInit {
  
  @Input() aula: Aula = {
    id: '',
    materia: '',
    aula: '',
    link: [],
    material: ''
  }
  constructor() { }

  ngOnInit(): void {
  }

}
