import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcluirClassesComponent } from './excluir-classes.component';

describe('ExcluirClassesComponent', () => {
  let component: ExcluirClassesComponent;
  let fixture: ComponentFixture<ExcluirClassesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExcluirClassesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExcluirClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
