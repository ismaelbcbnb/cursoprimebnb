import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-classes',
  templateUrl: './list-classes.component.html',
  styleUrls: ['./list-classes.component.css']
})
export class ListClassesComponent implements OnInit {

  listaMaterias= [
    {id: 1, nome:  'Apresentação do Curso'},
    {id: 2, nome:  'Plano de Estudos | Cronograma Eficiente'},
    {id: 3, nome:  'Português - Sérgio Rosa'},
    {id: 4, nome:  'Matemática Básica - Pedro Evaristo'},
    {id: 5, nome:  'Matemática Básica - Virginia Paulino'},
    {id: 6, nome:  'Matemática Financeira - Pacífico'},
    {id: 7, nome:  'Conhecimentos Bancários - Sirlo Oliveira'},
    {id: 8, nome:  'Aspectos Jurídicos - Rozângela Kemp'},
    {id: 9, nome:  'Legislação Especial - Victor Vieira'},
    {id: 10, nome:  'Legislação Aplicada ao BNB - Jander'},
    {id: 11, nome:  'Edital Verticalizado'},
    {id: 12, nome: 'Mapa da Aprovação'},
    {id: 13, nome: 'Exercícios / Lives'},
    {id: 14, nome: 'Simulado'},
  ]
   
  
  
  

  constructor() { }

  ngOnInit(): void {
  }

}
