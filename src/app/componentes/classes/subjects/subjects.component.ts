import { Component, Input, OnInit } from '@angular/core';
import { Aula } from '../aula';
import { ClassService } from '../class.service';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.css']
})
export class SubjectsComponent implements OnInit {

  @Input() materia = 
    {
      id: 0,
      nome: ''
    }
  

  listaClasses: Aula[] = [];
  
  constructor(private service: ClassService) { }

  ngOnInit(): void {
    this.service.listar().subscribe((listaClasses) =>{
      this.listaClasses = listaClasses
    })
  }

}
