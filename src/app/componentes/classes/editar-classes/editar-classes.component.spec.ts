import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarClassesComponent } from './editar-classes.component';

describe('EditarClassesComponent', () => {
  let component: EditarClassesComponent;
  let fixture: ComponentFixture<EditarClassesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarClassesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditarClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
