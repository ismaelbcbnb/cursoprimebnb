import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateClassesComponent } from './componentes/classes/create-classes/create-classes.component';
import { ListClassesComponent } from './componentes/classes/list-classes/list-classes.component';
import { ExcluirClassesComponent } from './componentes/classes/excluir-classes/excluir-classes.component';
import { EditarClassesComponent } from './componentes/classes/editar-classes/editar-classes.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'listarAula',
    pathMatch:'full'
  },
  {
    path: 'criarAula',
    component: CreateClassesComponent
  },
  {
    path: 'listarAula',
    component: ListClassesComponent
  },
  {
    path: 'classes/excluirAula/:id',
    component: ExcluirClassesComponent
  },
  {
    path: 'classes/editarAula/:id',
    component: EditarClassesComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
